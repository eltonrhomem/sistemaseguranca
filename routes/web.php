<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


// Route::group([ 'middleware' => 'api', 'namespace' => 'App\Http\Controllers', 'prefix' => 'auth'], 

// function ($router) {

   
//     Route::post('logout', 'AuthController@logout');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::post('me', 'AuthController@me');

// });


// Route::group([ 'namespace' => 'App\Http\Controllers', 'prefix' => 'api'], 

// function ($router) {

//     $router->post('/login', 'AuthController@login');
//     $router->post('/logout', 'AuthController@logout');
//     $router->post('/refresh', 'AuthController@refresh');
//     $router->post('/me', 'AuthController@me');

// });

$router->group(['prefix'=> 'usuario'], function () use($router){
    $router->post('cadastrar','UsuarioController@cadastrar');
});




$router->post('/login', 'AuthController@login');
$router->post('/info', 'AuthController@mostrarUsuarioAutenticado');
$router->post('/logout', 'AuthController@usuarioLogout');