<?php

//************************************************************************************** */


// Esse arquivo foi copiado seguindo 
// o passo a passo da URL https://jwt-auth.readthedocs.io/en/docs/quick-start/


//************************************************************************************** */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Usuario;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Auth\Authenticatable;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Support\Facades\Hash;




class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
       // $this->middleware('auth:api', ['except' => ['login']]);
       $this->jwt = $jwt;
       $this->middleware('auth:api', [
                        'except' => ['login']
       ]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        

        $this->validate($request, [
            'login' => 'required|max:100',
            'password' =>'required',
        ]);
        
       
        $credentials = $request->only('login', 'password');
        
        if (! $token = $this->jwt->attempt($request->only('login', 'password')) )
        {
            return response()->json(["Usuário não encontrado"], 400);
        }
        
        return response()->json(compact('token'));
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    public function mostrarUsuarioAutenticado()
    {
        $usuario = Auth::user();
        
        return response()->json($usuario);
    }

    public function usuarioLogout()
    {
        Auth::logout();

        return response()->json("Usuário logoff");
    }
}