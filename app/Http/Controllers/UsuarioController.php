<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   // protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
      // $this->jwt = $jwt;
    }

    public function cadastrar (Request $request) 
    {
        $usuario = new Usuario();
        $usuario->fill($request->all());
        $usuario->password = Hash::make($usuario->password);
        $usuario->save();

        return response()->json("Cadastrado o usuario");
    }

    //
}
