<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatetableSegurancaUsuarioC extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguranca_usuario_c', function (Blueprint $table){
            $table->increments('idusuario')->nullable(false);
            $table->string('nomeusuario',100)->nullable(false);
            $table->string('login',100)->nullable(false);
            $table->string('password',100)->nullable(true);
            $table->string('email', 100)->nullable(false);
            $table->char('cpfusuario',11)->nullable(false);
            $table->timestamps();   
            //$table->primary('idusuario');
            $table->unique('login');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seguranca_usuario_c');
    }
}
